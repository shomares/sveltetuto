import { writable } from "svelte/store";
import type { FeedBack } from "./model/feedback";

export const FeedBackStore = writable<FeedBack[]>([
    {
      id: "1",
      rating: 10,
      text: "1orem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin dolor eget pretium ultricies. Sed posuere mi vitae urna dictum mollis. Nunc non ex orci. Fusce lobortis ligula id mauris fermentum, in laoreet quam tincidunt. Sed eget lorem orci. Suspendisse vulputate felis elit, id facilisis massa rhoncus consequat. Integer quis diam metus. Integer risus arcu, pharetra sit amet orci id, pharetra egestas ex. Nam porta porta bibendum. Maecenas quam diam, tempor eget tortor vitae, ullamcorper finibus metus. Nunc odio augue, porta quis pretium quis, feugiat vel nisi. Morbi quis sem turpis. In id leo volutpat, laoreet libero quis, ornare augue.",
    },
    {
      id: "2",
      rating: 7,
      text: "2orem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin dolor eget pretium ultricies. Sed posuere mi vitae urna dictum mollis. Nunc non ex orci. Fusce lobortis ligula id mauris fermentum, in laoreet quam tincidunt. Sed eget lorem orci. Suspendisse vulputate felis elit, id facilisis massa rhoncus consequat. Integer quis diam metus. Integer risus arcu, pharetra sit amet orci id, pharetra egestas ex. Nam porta porta bibendum. Maecenas quam diam, tempor eget tortor vitae, ullamcorper finibus metus. Nunc odio augue, porta quis pretium quis, feugiat vel nisi. Morbi quis sem turpis. In id leo volutpat, laoreet libero quis, ornare augue.",
    },
    {
      id: "3",
      rating: 6,
      text: "3orem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin dolor eget pretium ultricies. Sed posuere mi vitae urna dictum mollis. Nunc non ex orci. Fusce lobortis ligula id mauris fermentum, in laoreet quam tincidunt. Sed eget lorem orci. Suspendisse vulputate felis elit, id facilisis massa rhoncus consequat. Integer quis diam metus. Integer risus arcu, pharetra sit amet orci id, pharetra egestas ex. Nam porta porta bibendum. Maecenas quam diam, tempor eget tortor vitae, ullamcorper finibus metus. Nunc odio augue, porta quis pretium quis, feugiat vel nisi. Morbi quis sem turpis. In id leo volutpat, laoreet libero quis, ornare augue.",
    },
  ])