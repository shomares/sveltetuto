export interface FeedBack {
    id: string,
    rating: number,
    text: string,
}